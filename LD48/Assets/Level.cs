using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public int LevelNumber = 0;
    public bool IsCatharsis = false;
    public StartingPoint startingPoint { get; private set; }

    void Awake()
    {
        startingPoint = GetComponentInChildren<StartingPoint>();
        Debug.AssertFormat(startingPoint != null || LevelNumber < 0, "Level {0} ({1}) missing starting point and is not a dummy level", LevelNumber, gameObject.name);
        goals = GetComponentsInChildren<Goal>();
        demons = GetComponentsInChildren<Demon>();
        hints = GetComponentsInChildren<Hint>();
    }

    public void Transition()
    {
        Debug.LogFormat("Transitioning level {0}...", LevelNumber);

        foreach(var goal in goals)
        {
            goal.Transition();
        }

        foreach(var demon in demons)
        {
            demon.Transition();
        }

        foreach(var hint in hints)
        {
            hint.GetComponentInChildren<Renderer>().enabled = false;
        }
    }

    public Vector3 GetTransitionStartingPoint()
    {
        foreach (var goal in goals)
        {
            if (!goal.IsTransitionGoal)
            {
                var pt = goal.transform.position;
                pt.z = transform.position.z;    // since goals are recessed a bit
                return pt;
            }
        }
        Debug.LogError("whoops");
        return Vector3.zero;
    }

    [HideInInspector]
    public Goal[] goals;
    Demon[] demons;
    Hint[] hints;
}
