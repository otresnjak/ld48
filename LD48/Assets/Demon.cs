using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demon : MonoBehaviour
{
    public AnimationCurve ScaleCurve;
    public bool CanHit { get; private set; } = false;

    public float WakeRadius = 8.0f;

    public float AttractionRadius = 5.0f;
    public float AttractionForce = 5.0f;
    public AnimationCurve AttractionForceAttenuation;

    public string CaptionOnSpawn;
    public string CaptionOnEncounterPlayer;
    public string[] CaptionOnDefeatPlayer;
    public string[] PlayerCaptionOnRespawn;

    public GameObject EffectSphere;
    public AnimationCurve EffectSphereSizeCurve;

    public void Transition()
    {
        captions.DestroyCaption();
        transform.parent.gameObject.SetActive(false);    // because of clunky demon prefab setup
    }

    void Awake()
    {
        baseScale = transform.localScale.x; // uniform scale so we only need 1 component
        captions = GetComponentInChildren<CaptionOwner>();
        EffectSphere.GetComponent<Renderer>().enabled = false;
        captions.ShowCaption(CaptionOnSpawn);
    }

    void FixedUpdate()
    {
        if (awoken)
        {
            float scale = baseScale * ScaleCurve.Evaluate(t);
            t += Time.fixedDeltaTime;
            if (t > 1.0f)
            {
                t -= 1.0f;
                CanHit = true;
            }
            transform.localScale = new Vector3(scale, scale, scale);

            float sphereScale = EffectSphereSizeCurve.Evaluate(t) * AttractionRadius * 2.0f;
            EffectSphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);

            var nearby = Physics.OverlapSphere(transform.position, AttractionRadius);
            foreach (var result in nearby)
            {
                var player = result.GetComponent<Player>();
                if (player != null)
                {
                    Vector3 attractionDirection = (transform.position - result.transform.position).normalized;
                    float distanceFactor = 1.0f - Vector3.Distance(transform.position, result.transform.position) / AttractionRadius;
                    distanceFactor = AttractionForceAttenuation.Evaluate(1.0f - distanceFactor);

                    var force = AttractionForce * distanceFactor * attractionDirection;

                    if (Player.Instance.Assist)
                        force *= 0.5f;

                    if (player.IsBoosting)
                        force *= player.AttractionForceAttenuationWhenBoosting;

                    result.GetComponent<Rigidbody>().AddForce(force);
                }
            }
        }

        if (!awoken)
        {
            if (Vector3.Distance(transform.position, Player.Instance.transform.position) < WakeRadius)
            {
                captions.ShowCaption(CaptionOnEncounterPlayer);
                EffectSphere.GetComponent<Renderer>().enabled = true;
                awoken = true;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();
        if (player != null)
        {
            if (CanHit && player.state == PlayerState.Normal)
            {
                bool killed = player.TakeHit(this);
                CanHit = false;

                if (killed)
                {
                    if (CaptionOnDefeatPlayer.Length > 0)
                    {
                        captions.ShowCaption(CaptionOnDefeatPlayer[currentDefeatCaption]);
                        if (currentDefeatCaption < CaptionOnDefeatPlayer.Length - 1)
                            ++currentDefeatCaption;
                    }
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttractionRadius);
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, WakeRadius);
    }

    float t = 0.0f;
    float baseScale;
    CaptionOwner captions;

    bool awoken = false;

    int currentDefeatCaption = 0;
    [HideInInspector] public int currentRespawnCaption = 0;
}
