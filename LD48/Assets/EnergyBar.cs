using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public Player CurrentPlayer;
    public Image EnergyBarImage;
    [Range(0.0f, 1.0f)] public float FillAmount = 1.0f;

    void Update()
    {
        if (CurrentPlayer != null)
        {
            displayedEnergy = Mathf.SmoothDamp(displayedEnergy, CurrentPlayer.Energy, ref energybarvel, 0.3f);
            EnergyBarImage.fillAmount = displayedEnergy;
        }
    }

    float displayedEnergy = 1.0f;
    float energybarvel = 0.0f;
}
