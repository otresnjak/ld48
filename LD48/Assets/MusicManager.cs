using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager Instance { get; private set; }

    [FMODUnity.EventRef] public string DescentMusic;
    [FMODUnity.EventRef] public string AscentMusic;
    [FMODUnity.EventRef] public string CatharsisMusic;
    [FMODUnity.EventRef] public string CreditsMusic;

    public bool Credits = false;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (Credits)
            PlayCreditsMusic();
        else
            PlayDescentMusic();
    }

    public void PlayDescentMusic()
    {
        currentMusic = FMODUnity.RuntimeManager.CreateInstance(DescentMusic);
        currentMusic.start();
    }

    public void PlayAscentMusic()
    {
        currentMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        currentMusic = FMODUnity.RuntimeManager.CreateInstance(AscentMusic);
        currentMusic.start();
    }

    public void PlayCatharsisMusic()
    {
        currentMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        currentMusic = FMODUnity.RuntimeManager.CreateInstance(CatharsisMusic);
        currentMusic.start();
    }

    public void PlayCreditsMusic()
    {
        currentMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        currentMusic = FMODUnity.RuntimeManager.CreateInstance(CreditsMusic);
        currentMusic.start();
    }

    public void StopMusic()
    {
        currentMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    FMOD.Studio.EventInstance currentMusic;
}
