using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class HoldRing : MonoBehaviour
{
    public InputActionAsset Input;
    public float HoldTime = 2.0f;
    public float WaitTimeAfterHold = 0.5f;
    public string NextScene;
    float DoneAnimDuration = 0.2f;
    float DoneAnimMaxScale = 1.5f;

    void Awake()
    {
        image = GetComponentInChildren<Image>();
        var map = Input.FindActionMap("UI");
        map.Enable();
        hold = map.FindAction("Continue");
        quit = map.FindAction("Quit");
    }

    void Update()
    {
        if (!done)
        {
            if (hold.ReadValue<float>() > 0.2f)
            {
                held += Time.deltaTime;
            }
            else
            {
                held -= Time.deltaTime;
                if (held < 0.0f) held = 0.0f;
            }
        }

        image.fillAmount = Mathf.Clamp01(held / HoldTime);

        if (done)
        {
            float t = doneTime / DoneAnimDuration;

            var color = image.color;
            color.a = 1.0f - t;
            image.color = color;

            float scale = Mathf.Lerp(1.0f, DoneAnimMaxScale, t);
            image.transform.localScale = new Vector3(scale, scale, scale);

            doneTime += Time.deltaTime;
        }

        if (!loading && held > HoldTime)
        {
            done = true;
            loading = true;
            Invoke("GoToNext", WaitTimeAfterHold);
        }

        if (quit.ReadValue<float>() > 0.5f)
            Application.Quit();
    }

    void GoToNext()
    {
        if (MusicManager.Instance != null)
            MusicManager.Instance.StopMusic();
        SceneManager.LoadSceneAsync(NextScene);
    }

    float doneTime = 0.0f;
    bool done = false;
    bool loading = false;
    float held = 0.0f;
    InputAction hold;
    InputAction quit;
    Image image;
}
