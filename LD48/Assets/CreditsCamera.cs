using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsCamera : MonoBehaviour
{
    void Awake()
    {
        var allCredits = FindObjectsOfType<Credit>();
        foreach(var c in allCredits)
        {
            credits.Add(c.Index, c);
        }

        curCredit = 0;
        toNextCredit = credits[curCredit].Duration;
    }

    void FixedUpdate()
    {
        toNextCredit -= Time.fixedDeltaTime;

        if (toNextCredit <= 0.0f)
        {
            if (curCredit + 1 < credits.Count)
            {
                ++curCredit;
                toNextCredit = credits[curCredit].Duration;
            }
        }

        var target = credits[curCredit];
        var pos = transform.position;
        Vector2 pos2d = pos;
        Vector2 target2D = target.transform.position;
        Vector3 newPos = Vector2.SmoothDamp(pos2d, target2D, ref velocity, 0.3f);
        newPos.z = pos.z;
        transform.position = newPos;

        float distToTarget = Vector2.Distance(pos2d, target2D);
        if (distToTarget < 1.0f)
            credits[curCredit].AnimateLines();
    }

    int curCredit = 0;
    Dictionary<int, Credit> credits = new Dictionary<int, Credit>();
    float toNextCredit;
    Vector2 velocity;
}
