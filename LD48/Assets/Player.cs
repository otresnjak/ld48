using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public enum PlayerState
{
    Normal,
    Respawning,
    Descending,
    Meeting,
    Joining,
    Ascending,
}

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    public int StartingLevel = 0;
    public bool SkipDialogue = false;
    public bool StartPostTransition = false;
    public bool Assist = false;

    public InputActionAsset Controls;
    public float Energy { get; set; } = 1.0f;
    public float EnergyDepletionRate = 0.5f;
    public float AttractionForceAttenuationWhenBoosting = 0.5f;
    public float BoostScale = 1.2f;
    public float RespawnDelay = 2.0f;

    public string[] CaptionOnRespawn;
    int nextRespawnCaption = 0;

    public TrailRenderer initialTrailRenderer;
    public TrailRenderer finalTrailRenderer;

    public Material TransitionMaterial;

    public int Hits = 0;
    int MaxHits = 1;

    public float MovementForceMagnitude = 6.0f;

    public bool IsBoosting { get; private set; } = false;

    void Awake()
    {
        Instance = this;

        trail = initialTrailRenderer;

        currentLevel = StartingLevel;

        captions = GetComponent<CaptionOwner>();
        trail.enabled = false;

        var playerControls = Controls.FindActionMap("Player Controls");
        playerControls.Enable();
        body = GetComponent<Rigidbody>();

        moveAction = playerControls.FindAction("Movement");
        primaryAction = playerControls.FindAction("Primary");
    }

    void Start()
    {
        if (StartPostTransition)
            LevelManager.Instance.Transition();
        trail.enabled = true;
        MoveToStart();
    }

    void MoveToStart()
    {
        transform.position = LevelManager.Instance.GetStartingPoint(currentLevel, transitioned);
        trail.Clear();
    }

    void FixedUpdate()
    {
        if (state == PlayerState.Normal)
        {
            float moveForce = MovementForceMagnitude;

            float primaryActionInput = primaryAction.ReadValue<float>();
            if (primaryActionInput > 0.0f && Energy > 0.0f)
            {
                moveForce *= Mathf.Lerp(0.0f, BoostScale, primaryActionInput);
                IsBoosting = true;

                Energy -= Time.fixedDeltaTime * primaryActionInput * EnergyDepletionRate;
                Energy = Mathf.Clamp01(Energy);
            }

            Vector2 moveInput = moveAction.ReadValue<Vector2>();
            moveInput.Normalize();

            body.AddForce(moveInput * moveForce, ForceMode.Acceleration);

            if (Hits >= MaxHits)
            {
                StartRespawn();
            }
        }
    }

    void Update()
    {
        //Debug.Log(state + "  level " + currentLevel);
        if (state == PlayerState.Descending)
        {
            var nextStartingPoint = LevelManager.Instance.GetStartingPoint(currentLevel + 1, state == PlayerState.Ascending);
            Vector3 newPos = Vector3.SmoothDamp(transform.position, nextStartingPoint, ref descentSpeed, 1.0f);
            transform.position = newPos;

            if (Vector3.Distance(transform.position, nextStartingPoint) < 0.5f)
            {
                state = PlayerState.Normal;
                ++currentLevel;
                if (LevelManager.Instance.GetLevel(currentLevel).IsCatharsis)
                    MusicManager.Instance.PlayCatharsisMusic();
            }
        }

        if (state == PlayerState.Ascending)
        {
            var nextStartingPoint = LevelManager.Instance.GetLevel(currentLevel - 1).GetTransitionStartingPoint();
            Vector3 newPos = Vector3.SmoothDamp(transform.position, nextStartingPoint, ref descentSpeed, 1.0f);
            transform.position = newPos;

            if (Vector3.Distance(transform.position, nextStartingPoint) < 0.5f)
            {
                state = PlayerState.Normal;
                --currentLevel;

                if (currentLevel == -1)
                {
                    MusicManager.Instance.StopMusic();
                    SceneManager.LoadSceneAsync("Credits");
                }
            }
        }

        if (state == PlayerState.Joining)
        {
            Vector3 newPos = Vector3.SmoothDamp(transform.position, joinTarget, ref descentSpeed, joinSmoothTime);
            transform.position = newPos;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        var goal = other.GetComponent<Goal>();
        if (goal != null && state == PlayerState.Normal)
        {
            if (goal.IsTransitionGoal)
            {
                Energy = 1.0f;
                state = PlayerState.Ascending;
            }
            else
            {
                Energy = 1.0f;
                state = PlayerState.Descending;
                if (LevelManager.Instance.GetLevel(currentLevel + 1).IsCatharsis)
                    MusicManager.Instance.StopMusic();
            }
        }
    }

    void StartRespawn()
    {
        state = PlayerState.Respawning;
        body.velocity = Vector3.zero;
        SetEnabled(false);
        body.isKinematic = true;
        Invoke(nameof(FinishRespawn), RespawnDelay);
    }

    void FinishRespawn()
    {
        Hits = 0;
        Energy = 1.0f;
        MoveToStart();
        SetEnabled(true);
        body.velocity = Vector3.zero;
        state = PlayerState.Normal;
        captions.ShowCaption(CaptionOnRespawn[nextRespawnCaption++], 3.0f);
        if (nextRespawnCaption >= CaptionOnRespawn.Length) nextRespawnCaption = 0;
        body.isKinematic = false;
    }

    void SetEnabled(bool value)
    {
        GetComponent<Renderer>().enabled = value;
    }

    public bool TakeHit(Demon culprit)
    {
        if (Hits < MaxHits)
        {
            Hits += 1;
            if (Hits >= MaxHits)
            {
                if (culprit.PlayerCaptionOnRespawn.Length > 0)
                {
                    if (culprit.PlayerCaptionOnRespawn[culprit.currentRespawnCaption] != "")
                    {
//                        CaptionOnRespawn = culprit.PlayerCaptionOnRespawn[culprit.currentRespawnCaption];
                        if (culprit.currentRespawnCaption < culprit.PlayerCaptionOnRespawn.Length - 1)
                            ++culprit.currentRespawnCaption;
                    }
                }
            }
        }
        return (Hits >= MaxHits);
    }

    public void ChangeTrailColor()
    {
        finalTrailRenderer.gameObject.SetActive(true);
        initialTrailRenderer.gameObject.SetActive(false);
    }

    public void BeginJoin(Vector3 target, float smoothtime)
    {
        joinTarget = target;
        body.isKinematic = true;
        joinSmoothTime = smoothtime;
        state = PlayerState.Joining;
    }

    public void FinishJoin()
    {
        body.isKinematic = false;
        state = PlayerState.Normal;
        ChangeTrailColor();
        GetComponent<Renderer>().material = TransitionMaterial;
        transitioned = true;

        // @todo - reconfigure levels here
    }

    Rigidbody body;
    InputAction moveAction;
    InputAction primaryAction;

    Vector3 descentSpeed = Vector3.zero;

    public int currentLevel = 0;
    public PlayerState state = PlayerState.Normal;

    Vector3 joinTarget;
    float joinSmoothTime;

    public CaptionOwner captions;
    bool transitioned = false;

    TrailRenderer trail;
}
