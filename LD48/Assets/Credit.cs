using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credit : MonoBehaviour
{
    public int Index = 0;
    public float Duration = 5.0f;
    float LineInterval = 1.0f;
    public int AnimateStartLine = 1;
    public GameObject ActivateWhenStartingAnimation;
    public float ActivateWhenStartingAnimationDelay = 1.0f;

    void Awake()
    {
        textLines = GetComponentsInChildren<TMPro.TMP_Text>();
        curTexLine = AnimateStartLine - 1;
        for (int i = AnimateStartLine; i < textLines.Length; ++i)
        {
            textLines[i].gameObject.SetActive(false);
        }
    }

    public void AnimateLines()
    {
        if (!animating)
        {
            animating = true;
            Invoke(nameof(ShowNextLine), LineInterval);

            if (ActivateWhenStartingAnimation != null)
            {
                Invoke(nameof(ActivateThingie), ActivateWhenStartingAnimationDelay);
            }
        }
    }

    void ActivateThingie()
    {
        ActivateWhenStartingAnimation.SetActive(true);
    }

    void ShowNextLine()
    {
        curTexLine += 1;
        if (curTexLine < textLines.Length)
        {
            textLines[curTexLine].gameObject.SetActive(true);
            Invoke(nameof(ShowNextLine), LineInterval);
        }
    }

    int curTexLine = 0;
    bool animating = false;

    TMPro.TMP_Text[] textLines;
}
