using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angel : MonoBehaviour
{
    void Awake()
    {
        captions = GetComponentInChildren<CaptionOwner>();
        captions.ShowCaption("I understand how you feel.");
    }

    void Update()
    {
    }

    CaptionOwner captions;
}
