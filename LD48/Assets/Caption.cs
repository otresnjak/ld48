using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Caption : MonoBehaviour
{
    public GameObject Owner;
    public float hOffset = 14.5f;
    public float zBase = 0.0f;
    public float Lifespan = 3.0f;

    public string Text {
        get
        {
            return text.text;
        } 
        set
        {
            text.text = value;
        }
    }

    void Awake()
    {
        text = GetComponentInChildren<TMPro.TMP_Text>();
    }

    void Start()
    {
        UpdatePosition();
    }

    void Update()
    {
        if (Lifespan > 0.0f)
        {
            age += Time.deltaTime;
            if (age > Lifespan)
                Destroy(gameObject);
        }
    }

    //void LateUpdate()
    //{
    //    UpdatePosition();
    //}

    void UpdatePosition()
    {
        Vector3 pos = Owner.transform.position;
        pos.x += hOffset;
        pos.z = zBase;
        transform.position = pos;
    }

    float age = 0.0f;
    TMPro.TMP_Text text;
}
