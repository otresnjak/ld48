using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public Vector3 GetStartingPoint(int level, bool transition)
    {
        if (levels.ContainsKey(level))
        {
            if (transition)
            {
                return levels[level].GetTransitionStartingPoint();
            }
            else
            {
                return levels[level].startingPoint.transform.position;
            }
        }

        Debug.LogErrorFormat("level number {0} not found", level);
        return Vector3.zero;
    }

    public Level GetLevel(int level)
    {
        if (levels.ContainsKey(level))
        {
            return levels[level];
        }
        Debug.LogErrorFormat("level number {0} not found", level);
        return null;
    }

    public void Transition()
    {
        foreach(var level in levels.Values)
        {
            level.Transition();
        }
    }

    void Awake()
    {
        Instance = this;
        var levelComponents = FindObjectsOfType<Level>();
        foreach(var level in levelComponents)
        {
            Debug.AssertFormat(!levels.ContainsKey(level.LevelNumber), "duplicate level index {0}", level.LevelNumber);
            levels.Add(level.LevelNumber, level);
        }
    }

    Dictionary<int, Level> levels = new Dictionary<int, Level>();
}
