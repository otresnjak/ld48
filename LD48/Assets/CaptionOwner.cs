using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptionOwner : MonoBehaviour
{
    public Caption CaptionPrefab;

    void Awake()
    {
    }

    public void ShowCaption(string text, float lifespan = -1.0f)
    {
        if (text != "")
        {
            DestroyCaption();

            var caption = Instantiate(CaptionPrefab, transform.position, Quaternion.identity);
            caption.Text = text;
            caption.Owner = gameObject;
            caption.Lifespan = lifespan;

            GameObject referenceObject = null;
            var level = GetComponentInParent<Level>();
            if (level != null) referenceObject = level.gameObject;
            if (referenceObject == null)
                referenceObject = gameObject;

            caption.zBase = referenceObject.transform.position.z;
            currentCaption = caption;
        }
    }

    public void DestroyCaption()
    {
        if (currentCaption) Destroy(currentCaption.gameObject);
    }

    Caption currentCaption;
}
