using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public float ActivationRadius = 5.0f;
    public Vector3 ActivationOffset = Vector3.zero;
//    public float AttractionForce = 5.0f;
    public string CaptionOnEncounter = "There must be a way...";
    public float FadeDuration = 0.5f;
    public bool IsTransitionGoal = false;

    void Awake()
    {
        captions = GetComponentInChildren<CaptionOwner>();
    }

    void Start()
    {
        if (IsTransitionGoal)
        {
            gameObject.SetActive(false);
        }
    }

    public void Transition()
    {
        if (IsTransitionGoal)
        {
            Debug.Log("Transitioning goal...");
            gameObject.SetActive(true);
            captions.ShowCaption(CaptionOnEncounter);
        }
        else
        {
            gameObject.SetActive(false);
            captions.DestroyCaption();
        }
    }

    void FixedUpdate()
    {
        if (!reached)
        {
            var nearby = Physics.OverlapSphere(transform.position + ActivationOffset, ActivationRadius);
            foreach (var result in nearby)
            {
                var player = result.GetComponent<Player>();
                if (player != null)
                {
                    //var force = AttractionForce * (transform.position - result.transform.position);
                    //result.GetComponent<Rigidbody>().AddForce(force);
                    captions = GetComponentInChildren<CaptionOwner>();
                    captions.ShowCaption(CaptionOnEncounter);
                }
            }
        }
        else
        {
            timeSinceReached += Time.fixedDeltaTime;
            float alpha = 1.0f - Mathf.Clamp01(timeSinceReached / FadeDuration);
            var renderer = GetComponent<Renderer>();
            var color = renderer.material.color;
            color.a = alpha;
            renderer.material.color = color;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!reached)
        {
            var player = other.GetComponent<Player>();
            if (player != null)
            {
                Util.ToFadeMode(GetComponent<Renderer>().material);
                reached = true;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position + ActivationOffset, ActivationRadius);
    }

    float timeSinceReached = 0.0f;
    bool reached = false;
    CaptionOwner captions;
}
