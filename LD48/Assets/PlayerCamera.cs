using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[SelectionBase]
public class PlayerCamera : MonoBehaviour
{
    public float InitialOffset = -50.0f;
    public float Offset = -100.0f;

    void Start()
    {
        var pos = LevelManager.Instance.GetStartingPoint(Player.Instance.StartingLevel, false);
        pos.z += InitialOffset;
        transform.position = pos;
    }

    void LateUpdate()
    {
        Transform target = Player.Instance.transform;

        var pos = transform.position;
        Vector2 pos2d = pos;
        Vector2 target2D = target.position;
        Vector3 newPos = Vector2.SmoothDamp(pos2d, target2D, ref velocity, 0.3f);

        float targetZ = target.position.z + Offset;
        newPos.z = Mathf.SmoothDamp(transform.position.z, targetZ, ref zvelocity, 0.3f);

        // clamp for final ascent
        if (newPos.z < Offset)
            newPos.z = Offset;

        transform.position = newPos;



        // don't pull focus for final ascent
        if (Player.Instance.currentLevel == 0 && Player.Instance.state == PlayerState.Ascending)
            return; 


        float distToPlayerZ = Mathf.Abs(transform.position.z - target.transform.position.z);
        var cam = GetComponentInChildren<Camera>();
        var volume = cam.gameObject.GetComponent<PostProcessVolume>();
        DepthOfField dof;
        volume.profile.TryGetSettings(out dof);
        dof.focusDistance.value = distToPlayerZ;
    }

    float zvelocity;
    Vector2 velocity;
}
