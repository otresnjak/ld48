using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SelfState
{
    Attract,
    Meet,
    Join,
    Pulse,
    Finished,
}

public enum DialogueSpeaker
{
    Player,
    Other,
}

[System.Serializable]
public class DialogueEntry
{
    public DialogueSpeaker Speaker;
    public string Text;
    public float Duration = 3.0f;
    public bool TriggerMusicChange;
    public bool TriggerJoin;
}

public class Self : MonoBehaviour
{
    public DialogueEntry[] Dialogue;
    public float JoinSmoothTime = 1.0f;
    public float JoinDuration = 2.0f;
    public float PulseFinalSize = 10.0f;

    void Awake()
    {
        body = GetComponent<Rigidbody>();
        captions = GetComponent<CaptionOwner>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (state == SelfState.Attract)
        {
            var player = collision.gameObject.GetComponent<Player>();
            if (player != null)
            {
                state = SelfState.Meet;
                player.state = PlayerState.Meeting;
                StartDialogue();
            }
        }
    }

    void FixedUpdate()
    {
        if (state == SelfState.Pulse)
        {
            float scale = transform.localScale.x;
            scale = Mathf.Lerp(scale, PulseFinalSize, 0.03f);
            transform.localScale = new Vector3(scale, scale, scale);
            float alpha = 1.0f - (scale / PulseFinalSize);

            var r = GetComponent<Renderer>();
            Color c = r.material.color;
            c.a = alpha;
            r.material.color = c;
        }
    }

    void Update()
    {
        if (state == SelfState.Join)
        {
            Vector3 newPos = Vector3.SmoothDamp(transform.position, joinTarget, ref joinSpeed, JoinSmoothTime);
            transform.position = newPos;
        }
    }

    void StartDialogue()
    {
        if (!Player.Instance.SkipDialogue)
        {
            ShowLine(0);
        }
        else
        {
            BeginJoin();
        }
    }

    void NextLine()
    {
        if (dialogueLine + 1 < Dialogue.Length)
        {
            ++dialogueLine;
            ShowLine(dialogueLine);
        }
    }

    void ShowLine(int line)
    {
        Debug.LogFormat("Showing line {0}", dialogueLine);

        if (Dialogue[line].Speaker == DialogueSpeaker.Other)
            captions.ShowCaption(Dialogue[line].Text);
        else
            Player.Instance.captions.ShowCaption(Dialogue[line].Text);

        if (Dialogue[line].TriggerMusicChange)
            MusicManager.Instance.PlayAscentMusic();
        if (Dialogue[line].TriggerJoin)
            BeginJoin();

        Invoke(nameof(NextLine), Dialogue[line].Duration);
    }

    void BeginJoin()
    {
        joinTarget = (transform.position + Player.Instance.transform.position) * 0.5f;
        Player.Instance.BeginJoin(joinTarget, JoinSmoothTime);
        state = SelfState.Join;
        GetComponent<Collider>().enabled = false;
        Invoke(nameof(FinishJoin), JoinDuration);
    }

    void FinishJoin()
    {
        Debug.Log("FINISH JOIN");
        Player.Instance.FinishJoin();
        gameObject.layer = LayerMask.NameToLayer("TransparentFX");
        Util.ToFadeMode(GetComponent<Renderer>().material);
        state = SelfState.Pulse;
        LevelManager.Instance.Transition();
    }
    
    void OnDrawGizmos()
    {
        if (state == SelfState.Join)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(joinTarget, 0.5f);
        }
    }

    int dialogueLine = 0;
    float toNextDialogueLine;

    Vector3 joinTarget;
    Vector3 joinSpeed;

    SelfState state = SelfState.Attract;
    CaptionOwner captions;
    Rigidbody body;
}
